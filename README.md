# CHEZ14's ApiGen Image for GitLab

ApiGen's original image is not actually compatible with GitLab thingy, so I made
this image to make it easier to implement it in my project's CI/CD.

The image is based on Alpine image of PHP to make it lighter!

Important notes:
- To make this image lighter, I decided to strip composer from this part. So, in
  order for ApiGen to add extra external documentations for libs, you will need
  a job that runs `composer install`, and cache them to this job.

## Usage

Sample `.gitlab-ci.yml` job that uses this image would looked like this:

```yml
stages:
  - test
  - build
  - deploy

.run-on-protected-context:
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'

## Install the vendor here, and run the tests
Test:
  image: registry.gitlab.com/net.christianto/images/php-testing:8.1
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - !reference [.run-on-protected-context, rules]
  before_script:
    - composer config --global "gitlab-oauth.$CI_SERVER_HOST" "$CI_JOB_TOKEN"
    - composer install --no-interaction --no-progress
  script:
    - composer run styles
  cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - vendor/

## Generate Documentations
Documentation Generate:
  stage: build
  image: registry.gitlab.com/net.christianto/images/apigen:edge-php8.2
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - !reference [.run-on-protected-context, rules]
  needs:
    - Test
  before_script:
    - !reference [Test, before_script] # Follow the Test' Before Script.
  script:
    - apigen
  artifacts:
    paths:
      - public/api/
  cache:
    policy: pull
    key: "test-$CI_COMMIT_REF_SLUG"
    paths:
      - vendor/

pages:
  stage: deploy
  image: alpine:latest
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  script:
    - cp -r docs/. public/
  artifacts:
    paths:
      - public/
```
## License

This specific repo are licensed as [MIT](./LICENSE). ApiGen's License are
[MIT](https://github.com/ApiGen/ApiGen/blob/master/LICENSE) too, but are
licensed by different party.
