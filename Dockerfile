ARG COMPOSER_VER=2
ARG APIGEN_VER=edge
ARG PHP_BASE_VER=8.1

################################################################################
## Dependencies from other image
################################################################################
FROM composer:$COMPOSER_VER as dep_composer

FROM apigen/apigen:$APIGEN_VER as dep_apigen


################################################################################
## Our main image
################################################################################
FROM php:$PHP_BASE_VER-alpine as main

################################################################################
## Basic Necessity
################################################################################
RUN apk update -q && \
    apk --no-cache add --virtual .build-dep ${PHPIZE_DEPS} && \
    apk --no-cache add icu-dev pcre-dev libsodium-dev && \
    docker-php-ext-install intl sodium opcache pcntl && \
    # install required items for igbinary
    pecl install igbinary && docker-php-ext-enable igbinary && \
    # Remove unneeded stuffs
    apk del .build-dep


################################################################################
## Setup the User first, switch later.
################################################################################
ARG USER=dokugen
RUN addgroup --system --gid 1000 ${USER} && \
    adduser --system --uid 1000 -G ${USER} ${USER}

################################################################################
## ApiGen Install
################################################################################
ARG APIGEN_HOME=/app/apigen/
ENV APIGEN_HOME=${APIGEN_HOME}

COPY --from=dep_apigen \
    --chown=${USER}:${USER} \
    /src ${APIGEN_HOME}

################################################################################
## Finalizing build.
################################################################################
ENV PATH=${PATH}:${COMPOSER_HOME}/vendor/bin/:${APIGEN_HOME}/bin/
USER ${USER}
WORKDIR /app
